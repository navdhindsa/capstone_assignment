<?php

/**
      * Capstone
      * @file config.php
      * @course Intro PHP, WDD 2018 Jan
      * @author Navdeep dhindsa <dhindsanavdeep24@gmail.com>
      * @created_at 2018-08-02
   */

ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
?>