
<?php
  /**
  * Capstone
  * @file nav.inc.php
  * @course Intro PHP, WDD 2018 Jan
  * @author Navdeep dhindsa <dhindsanavdeep24@gmail.com>
  * @created_at 2018-08-0
  */
?>
  <div id="wrapper">
      
      <header>
        <div id="header_left">
        <span id="logo">
          <img src="images/logonew.JPG" width="175" height="140" alt="Logo" />
        </span>
        
        <div id="tagline">
          <h1>
          <?=$title?>
          </h1>
        </div>
      </div> <!-- left header ends-->
        
        <nav>
          <div id="menu">
            <a href="#" id="menulink" title="Menu">
              <div class="bar1"></div>
              <div class="bar2"></div>
              <div class="bar3"></div>
            </a>
             
            <ul id="navlist">
              <li><a href="#"   title="Home">HOME</a></li><!--Home-->
              <li><a href="#"   title="About">ABOUT</a></li><!--Services-->
              <li><a href="#"    title="Skills">SKILLS</a></li><!--about us-->
              <li><a href="#"   title="FAQs">FAQs</a></li><!--contact us-->
              <li><a href="connect.php"   <?php if($slug == 'connect') {echo 'class="current"';}?>  title="Connect">CONNECT</a></li><!--reviews-->
            </ul>
          </div>
        </nav>
      </header>