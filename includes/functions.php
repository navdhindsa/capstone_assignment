<?php

/**
 * Validate for required
 * @param String $field_name
 * @param Array &$errors 
 * @return errors array if error otherwise nothing
 */
function required()
{
  $errors=null;
    if(empty($_POST['first_name'])) {
        $errors['first_name'] = "First Name is a required field."; 
      $flag=1;
    }
  
   if(empty($_POST['last_name'])) {
        $errors['last_name'] = "Last Name is a required field."; 
    }
  
   if(empty($_POST['street'])) {
        $errors['street'] = "Street is a required field."; 
    }
  
   if(empty($_POST['city'])) {
        $errors['city'] = "City is a required field."; 
    }
  
   if(empty($_POST['postal_code'])) {
        $errors['postal_code'] = "Postal Code is a required field."; 
    }
  
   if(empty($_POST['province'])) {
        $errors['province'] = "Province is a required field."; 
    }
  
   if(empty($_POST['country'])) {
        $errors['country'] = "Country is a required field."; 
    }
  
   if(empty($_POST['phone'])) {
        $errors['phone'] = "Phone is a required field."; 
    }
  
  if(empty($_POST['comment'])) {
        $errors['comment'] = "Comment is a required field."; 
    }
  
  if(empty($_POST['phone'])) {
        $errors['phone'] = "Phone is a required field."; 
    }
  
  if(empty($_POST['email'])) {
        $errors['email'] = "Email is a required field."; 
    }
  
  if(empty($_POST['age'])) {
        $errors['age'] = "Age is a required field."; 
    }
  
  if(empty($_POST['password'])) {
        $errors['password'] = "Password is a required field."; 
    }
  
  if(empty($_POST['confirm_password'])) {
        $errors['confirm_password'] = "Confirm Password is a required field."; 
    }
  return $errors;
  
}


/**
 * Validate Email
 * @param String $field_name
 * @param Array &$errors (passed by reference)
 * @return Bool true if valid, false otherwise
 */
function validateEmail($field_name, &$errors)
{

    if(!filter_input(INPUT_POST, $field_name, FILTER_VALIDATE_EMAIL)) {
        $errors[$field_name] = "Please enter a valid email address";
        return false;
    }
    return true;
}

/**
* Escape a string for use ingeneral HTML
* @param String $string the string to be escaped
* @return String the escaped string
*/
function esc($string) 
{
    return htmlspecialchars($string, NULL, 'UTF-8', false);
}

/**
* Escape a string for use in an attribute
* @param String $string the string to be escaped
* @return String the escaped string
*/
function esc_attr($string)
{
    return htmlspecialchars($string, ENT_QUOTES, "UTF-8", false);
}

