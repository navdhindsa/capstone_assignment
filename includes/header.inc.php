<!doctype html>
<html lang="en">
  <head>
    <title>Navdeep - Connect with Us</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <!-- favorites and apps icons -->
    <link rel="icon" type="image/png" sizes="16x16" href="images/favicon.png">
    <!-- embedding Google font -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:400,400i,700" rel="stylesheet">
 
    <!-- embedding stylesheets -->
    <link rel="stylesheet" type="text/css" href="styles/desktop.css" media="screen and (min-width:769px)" />
    <link rel="stylesheet" type="text/css" href="styles/mobile.css" media="screen and (max-width:768px)" />
    <link rel="stylesheet" type="text/css" href="styles/print.css" media="print" />
    
    <?php
    /**
      * Capstone
      * @file header.inc.php
      * @course Intro PHP, WDD 2018 Jan
      * @author Navdeep dhindsa <dhindsanavdeep24@gmail.com>
      * @created_at 2018-08-02
   */
    ?>
    <style>
		body nav ul#navlist li a.current {
			border-bottom: 2px solid #f7941e;
            border-top: 2px solid #f7941e;
            color: #35afbe;
		}
      .error{
        color: #a33;
      }
	</style>
    
    <!--
||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
||                                                            ||
||                                                            ||
||                                                            ||
||                .-.                                         ||
||       ,     .-.   .-.                                      ||
||      / \   (   )-(   )          WDD WDD Capstone Project   ||
||      \ |  ..>-( . )-<           By- Navdeep Kaur Dhindsa   ||
||       \|..'(   )-(   )          WDD PACE 2018              ||
||        Y ___`-'   `-'                                      ||
||        |/__/   `-'                                         ||
||        |                                                   ||
||        |                                                   ||
||        |                                                   ||
||  ______|________                                           || 
|| |_______________|                                          ||
||  \             /                                           ||
||   \           /                                            ||
||    \         /                                             ||
||     \       /                                              ||
||      \_____/                                               ||
||                                                            ||
||                                                            ||
||                                                            ||
||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||  
  
  
  
  
  -->
  
    <!-- conditions for IE9 and earlier support -->
    <!--[if LTE IE 9]>
      <link rel="stylesheet" type="text/css" href="styles/desktop.css" />
      <script>
        document.createElement('header');
        document.createElement('nav');
        document.createElement('footer');
        document.createElement('main');
        document.createElement('section');
      </script>

      <style type="text/css">
        header, nav, main, section, footer{
          display: block;
        }
      </style>
    <![endif]-->
    
    <!-- IEs to avoid borders on image links -->
    <style>
      a img{
        border: none;
        outline: none;
      }
    </style>
    <?php if($title = 'Navdeep -FAQs') : ?>

		<link rel="stylesheet" href="/styles/customcss.css" />

	<?php endif; ?>
    
  </head>


