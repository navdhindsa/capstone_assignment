<?php 

$title = 'Navdeep - Connect with Us';
$slug = 'connect';
require '../includes/connect_db.inc.php';
require '../config.php';
require '../includes/functions.php';

  /**
  * Capstone
  * @file connect.php
  * @course Intro PHP, WDD 2018 Jan
  * @author Navdeep dhindsa <dhindsanavdeep24@gmail.com>
  * @created_at 2018-08-02
  */

if($_SERVER['REQUEST_METHOD'] == 'POST') {

    $errors = [];
  $flag=0;


//    $errors = validateEmail('email', $errors);

 // $errors = passwordsMatch('password', 'confirm_password', $errors);
 
  if($_POST['password'] !== $_POST['confirm_password']) {
        $errors1 = 'Passwords do not match';
    $flag=1;
    }
  if(!filter_input(INPUT_POST, 'email', FILTER_VALIDATE_EMAIL)) {
        $errors2 = "Please enter a valid email address";
        $flag=1;
    }
  
  $errors=required();

  if(!is_numeric($_POST['age'])){
    $errors3= "Age is not a number";
    $flag =1;
  }
  if(is_numeric($_POST['age']) && $_POST['age']>110){
    $errors4= "We don't think you are that old!";
    $flag =1;
  }
  if(is_numeric($_POST['age']) && $_POST['age']<6){
    $errors5= "Please don't be so modest!";
    $flag =1;
  }
   
  if(!is_numeric($_POST['phone'])){
    $errors6= "Phone is not a number";
    $flag =1;
  }
  if(is_numeric($_POST['phone']) && strlen($_POST['phone'])!=10){
    $errors7= "Phone number should be 10 digits long!";
    $flag =1;
  }
  
    if(empty($errors )&& $flag!=1) {

        $dbh = new PDO(DB_DSN, DB_USER, DB_PASS);
        $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        $query = 'INSERT INTO users 
                (first_name, last_name, email, age, password, street, city, province, country, comment,  postal_code, phone)
                VALUES
                (:first_name, :last_name, :email, :age, :password, :street, :city, :province, :country, :comment,  :postal_code, :phone)';

        $stmt = $dbh->prepare($query);

        $params = array (
            ':first_name' => $_POST['first_name'],
            ':last_name' => $_POST['last_name'],
            ':email' => $_POST['email'],
            ':age' => intval($_POST['age']),
            ':password' => $_POST['password'],
            ':street' =>$_POST['street'], 
            ':city'=>$_POST['city'], 
            ':province' =>$_POST['province'], 
            ':country'=>$_POST['country'], 
            ':comment'=>$_POST['comment'], 
             ':phone'=>$_POST['phone'],
          ':postal_code'=>$_POST['postal_code']
       
            
        );


        if($stmt->execute($params)) {

            $id = $dbh->lastInsertId();

            $query = 'SELECT * FROM users
                    WHERE id = :id';
            $stmt = $dbh->prepare($query);

            $stmt->bindValue(':id', $id, PDO::PARAM_INT);

            $stmt->execute();

            $users = $stmt->fetch(PDO::FETCH_ASSOC);

            $success = true;

        } // end if stmt 

    } // end if no errors
    
} // end if POST submission






include '../includes/header.inc.php'; 
?>
  <body id="connect">
   <?php include '../includes/nav.inc.php' ?>    
          <div id="content">

      <!-- warning message for IE9 and earlier --> 
      <!--[if LT IE 9]>
        <div class="row">
          <div id="old_ie_warning"style="position:absolute; top:0px; ">
            <p>Some of the new features are not available for your broswer. Please upgrade it!</p>
          </div>
        </div>
      <![endif]-->
<?php if(empty($success)) : ?>
        <h2>Get in touch. </h2>
        <form id="email"
              method="post"
              action="connect.php"
              name="email"
              autocomplete="on" novalidate>
          <fieldset>
            <legend>Register</legend>
            <p>
              <label for="first_name">First Name </label>
              <input type="text"
                id="first_name" 
                name="first_name" 
                maxlength="25"
                size="30"
                placeholder="First Name" value="<?php if(!empty($_POST['first_name'])) 
                    echo esc_attr($_POST['first_name']); ?>"
                />
            </p>
            <?php  if(!empty($errors['first_name'])) : ?>
            <span class="error"><?=$errors['first_name']?></span>
             <?php endif; ?>
            <p>
              <label for="last_name">Last Name </label>
              <input type="text"
                id="last_name" 
                name="last_name" 
                maxlength="25"
                size="30"
                placeholder="Last Name" value="<?php if(!empty($_POST['last_name'])) 
                    echo esc_attr($_POST['last_name']); ?>"
                />
            </p>
            <?php if(!empty($errors['last_name'])) : ?>
            <span class="error"><?=$errors['last_name']?></span>
             <?php endif; ?>
            
            
            
            <p >
            <label  for="age">Age</label>
            <input type="text" name="age" maxlength="255"
            value="<?php if(!empty($_POST['age'])) 
                    echo esc_attr($_POST['age']); ?>" />
            
        </p>
            <?php if(!empty($errors['age'])) : ?>
            <span class="error"><?=$errors['age']?></span>
             <?php endif; ?>
            <?php if(!empty($errors3)) : ?>
            <span class="error"><?=$errors3?></span>
             <?php endif; ?>
            <?php if(!empty($errors4)) : ?>
            <span class="error"><?=$errors4?></span>
             <?php endif; ?>
            <?php if(!empty($errors5)) : ?>
            <span class="error"><?=$errors5?></span>
             <?php endif; ?>
            
            <p>
              <label for="street">Street Address </label>
              <input type="text"
                id="street" 
                name="street" 
                maxlength="25"
                size="30"
                placeholder="Street Address" value="<?php if(!empty($_POST['street'])) 
                    echo esc_attr($_POST['street']); ?>"
                />
            </p>
            <?php if(!empty($errors['street'])) : ?>
            <span class="error"><?=$errors['street']?></span>
             <?php endif; ?>
            <p>
              <label for="city">City</label>
              <input type="text"
                id="city" 
                name="city" 
                maxlength="25"
                size="30"
                placeholder="City" value="<?php if(!empty($_POST['city'])) 
                    echo esc_attr($_POST['city']); ?>"
                />
            </p>
            <?php if(!empty($errors['city'])) : ?>
            <span class="error"><?=$errors['city']?></span>
             <?php endif; ?>
            <p>
              <label for="province">Province</label>
              <input type="text"
                id="province" 
                name="province" 
                maxlength="25"
                size="30"
                placeholder="Province" value="<?php if(!empty($_POST['province'])) 
                    echo esc_attr($_POST['province']); ?>"
                />
            </p>
            <?php if(!empty($errors['province'])) : ?>
            <span class="error"><?=$errors['province']?></span>
             <?php endif; ?>
            <p>
              <label for="postal_code">Postal Code</label>
              <input type="text"
                id="postal_code" 
                name="postal_code" 
                maxlength="25"
                size="30"
                placeholder="A1A 1A1" value="<?php if(!empty($_POST['postal_code'])) 
                    echo esc_attr($_POST['postal_code']); ?>"
                />
            </p>
            <?php if(!empty($errors['postal_code'])) : ?>
            <span class="error"><?=$errors['postal_code']?></span>
             <?php endif; ?>
            
            <p>
              <label for="country">Country</label>
              <input type="text"
                id="country" 
                name="country" 
                maxlength="25"
                size="30"
                placeholder="Country" value="<?php if(!empty($_POST['country'])) 
                    echo esc_attr($_POST['country']); ?>"
                />
            </p>
            <?php if(!empty($errors['country'])) : ?>
            <span class="error"><?=$errors['country']?></span>
             <?php endif; ?>
            
            <p>
              <label for="email">Email Address </label> 
              <input type="email" 
                     name="email" 
                     id="email" value="<?php if(!empty($_POST['email'])) 
                    echo esc_attr($_POST['email']); ?>"
                     />
            </p>
            <?php if(!empty($errors['email'])) : ?>
            <span class="error"><?=$errors['email']?></span>
             <?php endif; ?>
            <?php if(!empty($errors2)) : ?>
            <span class="error"><?=$errors2?></span>
             <?php endif; ?>
            <p>
            <label for="phone">Telephone </label>
              <input type="text"
                     id="phone"
                     name="phone"
                     maxlength="18" value="<?php if(!empty($_POST['phone'])) 
                    echo esc_attr($_POST['phone']); ?>"
                      />
            </p>
              <?php if(!empty($errors['phone'])) : ?>
            <span class="error"><?=$errors['phone']?></span>
             <?php endif; ?>
               <?php if(!empty($errors6)) : ?>
            <span class="error"><?=$errors6?></span>
             <?php endif; ?>
            <?php if(!empty($errors7)) : ?>
            <span class="error"><?=$errors7?></span>
             <?php endif; ?>
            <p>
              <label for="password">Password</label>
              <input type="password"
                id="password" 
                name="password" 
                maxlength="25"
                size="30"
                placeholder="Password" 
                />
            </p>
            <?php if(!empty($errors['password'])) : ?>
            <span class="error"><?=$errors['password']?></span>
             <?php endif; ?>
        <?php if(!empty($errors1)) : ?>
            <span class="error"><?=$errors1?></span>
             <?php endif; ?>
            <p>
              <label for="confirm_password">Confirm Password</label>
              <input type="password"
                id="confirm_password" 
                name="confirm_password" 
                maxlength="25"
                size="30"
                placeholder="Confirm Password"
                />
            </p>
            <?php if(!empty($errors['confirm_password'])) : ?>
            <span class="error"><?=$errors['confirm_password']?></span>
             <?php endif; ?>
            <p>
              <label for="comment">Please leave your comments</label>  <br />
              <textarea cols="30" 
                        rows="6" 
                        id="comment" 
                        name="comment" value="<?php if(!empty($_POST['comment'])) 
                    echo esc_attr($_POST['comment']); ?>">
              </textarea>
            </p>
            <?php if(!empty($errors['comment'])) : ?>
            <span class="error"><?=$errors['comment']?></span>
             <?php endif; ?>
            <p>
              <input type="submit" 
                     name="submit" 
                     id="submit" 
                     value="Submit" 
              />
              &nbsp;&nbsp;
              <input type="reset" 
                     name="reset" 
                     id="reset" 
                     value="Reset" 
              />
              </p>

          </fieldset>
        </form>
    <?php else : ?>

    <h2>Thankyou for registering!</h2>

    <p>You have submitted the following Information:</p>

    <p>
        <strong>First Name</strong>: <?=esc($users['first_name'])?><br />
        <strong>Last Name</strong>: <?=esc($users['last_name'])?><br />
        <strong>Email</strong>: <?=esc($users['email'])?><br />
      <strong>Phone</strong>: <?=esc($users['phone'])?><br />
        <strong>Age</strong>: <?=esc($users['age'])?><br />
            <strong>Street</strong>: <?=esc($users['street'])?><br />
            <strong>City</strong>: <?=esc($users['city'])?><br />
            <strong>Postal Code</strong>: <?=esc($users['postal_code'])?><br />
            <strong>Province</strong>: <?=esc($users['province'])?><br />
            <strong>Country</strong>: <?=esc($users['country'])?><br />
            
            <strong>Comment</strong>: <?=esc($users['comment'])?><br />
            </p>

    <p>Back to <a href="connect.php">Registration Form</a></p>

  
    <?php endif; ?>

      </div>
      <?php include '../includes/footer.inc.php' ?>

